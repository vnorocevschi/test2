import React, {Component} from 'react';
import axios from 'axios';
import {
  Card, CardActionArea, CardContent,
  Container,
  Grid,
  Typography
} from "@material-ui/core";

interface Book {
  searchInfo: string;
  imageLinks?: { thumbnail: string };
  title: string;
  subtitle?: string;
}

class BookPage extends Component {
  state: { bookId: string; book: Book } = {
    bookId: '',
    book: {
      searchInfo: '',
      title: ''
    }
  };

  search = (bookId: string) => {
    axios.get("https://www.googleapis.com/books/v1/volumes/" + bookId)
      .then(res => {
        this.setState({
          book: {
            searchInfo: res.data.volumeInfo.searchInfo,
            title: res.data.volumeInfo.title,
            subtitle: res.data.volumeInfo.subtitle,
            imageLinks: res.data.volumeInfo.imageLinks,
          }
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          book: {
            searchInfo: '',
            title: ''
          }
        })
      });
    // this.setState({ people: this.DATASET.filter((user) => user.toLowerCase().includes(query.toLowerCase())) });
  };

  componentDidMount() {
    const href = window.location.href;
    const query = href.split('/')[href.split('/').length - 1];
    this.search(query);
  }

  render() {

    return (
      <Container maxWidth={false} fixed>
        <div style={{padding: 20}}>
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <Card>
                <CardActionArea>
                  {this.state.book.imageLinks && (
                    <img
                      style={{maxHeight: 250, objectFit: 'contain'}}
                      src={this.state.book.imageLinks.thumbnail}
                      alt={this.state.book.title}/>
                  )}
                  <CardContent>
                    <Typography gutterBottom variant="h5">
                      {this.state.book.title}
                    </Typography>
                    <Typography gutterBottom variant="h6">
                      {this.state.book.subtitle}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {this.state.book.searchInfo?.substring(0, 150)}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
        </div>
      </Container>
    );
  }
}

export default BookPage;
import React from "react";
import ReactDOM from "react-dom";
import './index.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import BookPage from "./BookPage";
import SearchPage from "./SearchPage";

ReactDOM.render(
  <Router>
    <React.StrictMode>
    <div>
      <Route exact path="/">
        <SearchPage />
      </Route>
      <Route path="/book/:id">
        <BookPage />
      </Route>
    </div>
    </React.StrictMode>
  </Router>,
  document.getElementById('root')
);
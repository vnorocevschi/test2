import React, {Component} from 'react';
import axios from 'axios';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Container,
  Grid, TextField,
  Typography
} from "@material-ui/core";
import {debounce} from 'lodash';

interface Book {
  id: string;
  volumeInfo: VolumeInfo;
}

interface VolumeInfo {
  searchInfo: string;
  imageLinks?: { thumbnail: string };
  title: string;
  subtitle?: string;
}

class SearchPage extends Component {
  state: { query: string; results: Book[] } = {
    query: "",
    results: []
  };

  onChange = (e: { target: { value: any; }; }) => {
    const {value} = e.target;
    this.setState({
      query: value
    });
    window.history.replaceState('', '', '?search=' + value);
    this.search(value);
  };

  search = debounce((query: string) => {
    axios.get("https://www.googleapis.com/books/v1/volumes?q=" + query)
      .then(res => this.setState({results: res.data.items}))
      .catch(err => {
        console.log(err);
        this.setState({results: []})
      });
  }, 200);

  componentDidMount() {
    const query = new URLSearchParams(window.location.search);
    const value: string = query.get('search') ? String(query.get('search')) : '';
    this.setState({query: value})
    this.search(value);
  }

  render() {

    return (
      <Container maxWidth={false} fixed>
        <div style={{padding: 20}}>
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <Box>
                <TextField label="Search" variant="outlined" onChange={this.onChange} value={this.state.query}/>

              </Box>
            </Grid>
            {this.state.results?.map(book => (
              <Grid item lg={3} md={4} sm={6} key={book.id}>
                <Card>
                  <a href={`/book/${book.id}`}>
                    {book.volumeInfo.imageLinks && (
                      <CardMedia
                        component="img"
                        style={{maxHeight: 250, objectFit: 'contain'}}
                        image={book.volumeInfo.imageLinks.thumbnail}
                        title={book.volumeInfo.title}
                      />
                    )}
                    <CardContent>
                      <Typography gutterBottom variant="h5">
                        {book.volumeInfo.title}
                      </Typography>
                      <Typography gutterBottom variant="h6">
                        {book.volumeInfo.subtitle}
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                        {book.volumeInfo.searchInfo?.substring(0, 150)}
                      </Typography>
                    </CardContent>
                  </a>
                  <CardActions>
                    <Button size="small" color="primary">
                      Favorite
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
            {/*{this.state.results.map(book => (*/}
            {/*  // <ul key={book}>*/}
            {/*  //   <li>{book}</li>*/}
            {/*  // </ul>*/}
            {/*))}*/}
          </Grid>
        </div>
      </Container>
    );
  }
}

export default SearchPage;